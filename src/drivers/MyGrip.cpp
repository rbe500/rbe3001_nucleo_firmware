#include "MyGrip.h"
#include "Clock.h"

// Parameterized class constructor
Grip::Grip(Servo * myServo)
{
  servo = myServo;
}

// // Return the current position of the system
// float PIDimp::getPosition( )
// {
//   return servo->read();
// }

// //Send controller signel to the motors, bounded and scaled by the configurations
// void PIDimp::setOutputLocal( float currentOutputValue)
// {
//   servo->write(currentOutputValue);
// }

// // return the current time in ms, this is needed by  the PID controller
// float PIDimp::getMs()
// {
//   return ((float)clock_us()) / 1000.0;
// }
