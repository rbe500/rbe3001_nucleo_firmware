/**
 * @file MyPid.h
 * @brief Implementation of a PID controller for the RBE3001 robotic arm
 * 
 * @section RBE3001 - Nucleo Firmware - MyPid.h
 *
 * Instructions
 * ------------
 * This class implements a PID controller. Default values for the controller
 * gains are defined below as C++ macros. You should have no need modify 
 * the code below, however you are highly encouraged to read it and see
 * the PID controller is implemented.
 * 
 */

#ifndef RBE3001_GRIPPER
#define RBE3001_GRIPPER

#include <PID_Bowler.h>
#include "Servo.h"
#include "RunEvery.h"

class Grip : public PIDBowler
{
 public:
  // when the constructor is called with no parameters, do nothing
  Grip(){}

  // constructor taking in the hardware objects
  Grip(Servo * myServo);
  
  // Functions inherited from PIDBowler
  // float getPosition();
  // void setOutputLocal(float);
  // float getMs();
  
  // Class public attributes
  Servo * servo;                // list of servo motors

  // Class private attributes
 private:
};

#endif
