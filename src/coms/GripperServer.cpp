/**
 * RBE3001 - Nucleo Firmware
 * See header file for more detail on this class.
 */
#include "GripperServer.h"

/**
 *  @brief This function handles incoming HID packets from MATLAB.
 *
 *  @description NaN
 *               NaN
 *               NaN
 */

void GripperServer::event(float * packet){

  float gripper_set = packet[1];
  servo->write(gripper_set);

  uint8_t * buff = (uint8_t *) packet;

  // re-initialize the packet to all zeros
  for (int i = 0; i < 60; i++)
      buff[i] = 0;

}

float GripperServer::getPosition(){

  return servo->read();

}