/**
 * @file GripperServer.h 
 * @brief GripperServer for the RBE3001 robotic arm
 * 
 * @section RBE3001 - Nucleo Firmware - GripperServer
 *
 * Instructions
 * ------------
 * This class implements a communication server that can be
 * used to send given setpoints (in joint space) to the robotic arm.
 * Setpoints generated in MATLAB and sent over HDI will be made available
 * to the `event()' function below. See the code in `PidServer.cpp' for
 * for more details.
 * 
 * IMPORTANT - Multiple communication servers can run in parallel, as shown
 *             in the main file of this firmware
 *             (see 'Part 2b' in /src/Main.cpp). To ensure that communication
 *             packets generated in MATLAB are routed to the appropriate 
 *             server, we use unique identifiers. The identifier for this
 *             server is the integer number 200.
 *             In general, the identifier can be any 4-byte unsigned
 *             integer number.
 */

#ifndef RBE3001_GRIPPER_SERVER
#define RBE3001_GRIPPER_SERVER

#include <PID_Bowler.h>
#include <PacketEvent.h>
#include "Servo.h"
#include <cmath>              // needed for std::abs

#define GRIPPER_SERVER_ID 200      // identifier for this server

/**
 *  @brief Class that receives setpoints through HID and sends them to
 *         the GRIPPER controller. Extends the `PacketEventAbstract' class.
 */  
class GripperServer: public PacketEventAbstract
{
 private:
  Servo * servo;
 public:
  GripperServer (Servo * myServo)
    : PacketEventAbstract(GRIPPER_SERVER_ID)
  {
    servo = myServo;
  }
  
  // This method is called every time a packet from MATLAB is received
  // via HID
  void event(float * buffer);
  float getPosition();
};

#endif /* end of include guard: RBE3001_GRIPPER_SERVER */
